import React from 'react'
import { Col, Card } from 'react-bootstrap';

export default function Techonology(props) {
    const { tech } = props;

    const { nombre, foto } = tech;
    
    return (
        <Col className="text-center">
            <Card className="project mt-4 text-center justify-content-center" style={{ width: '10rem' }}>
                <Card.Img variant="top" src={foto} />
                <Card.Body>
                    <Card.Text>{nombre}</Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}
