import React from 'react'
import { Card } from 'react-bootstrap';

export default function Work(props) {
    const { work } = props;
    const { titulo, empresa, fecha, responsabilidades } = work;

    return (
        <Card className="project mt-4">
            <Card.Header>
                <Card.Title>{titulo} - {empresa}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{fecha}</Card.Subtitle>
            </Card.Header>
            <Card.Body>
                {
                    responsabilidades.map((res, index) => (
                        <Card.Text key={index}>{res}</Card.Text>
                    ))
                }
            </Card.Body>
        </Card>
    )
}
