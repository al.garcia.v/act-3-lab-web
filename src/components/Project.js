import React from 'react'
import { Carousel, Card } from 'react-bootstrap';

function Project(props) {
    const { project } = props;
    const { nombre, tipo, fecha, descripcion, rol, link, fotosURLs } = project

    return (
        <Card className="project mt-4">
            <Card.Header>
                <Card.Title>{nombre} - {tipo}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{fecha}</Card.Subtitle>
            </Card.Header>
            <ImageCarousel 
                fotos={fotosURLs}
            />
            <Card.Body>
                <Card.Text>{descripcion}</Card.Text>
                <Card.Text>{rol}</Card.Text>
                <Card.Text><a href={link} target="_blank" rel="noreferrer">Ver projecto</a></Card.Text>
            </Card.Body>
        </Card>
    )
}

function ImageCarousel(props) {
    const { fotos } = props;
    
    return (
        <Carousel>
            {
                fotos.map((foto, index) => (
                    <Carousel.Item key={index}>
                        <img
                            className="d-block w-100"
                            src={foto}
                            alt={`slide ${index}`}
                        />
                    </Carousel.Item>
                ))
            }
        </Carousel>
    )
}

export default Project

