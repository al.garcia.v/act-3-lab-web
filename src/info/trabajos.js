export default function trabajos() {
    return [
        {
            "titulo": "Prácticante Software Engineer",
            "empresa": "IndexZero",
            "fecha": "Enero 2021 - Presente",
            "responsabilidades": [
                "Desarrollo de back-end y front-end en Ruby on Rails"
            ]
        },
        {
            "titulo": "Web Developer Intern",
            "empresa": "Carbono Dev",
            "fecha": "Junio 2019 - Agosto 2019",
            "responsabilidades": [
                "Implementé el API de You Need A Budget (YNAB) en Integromat para automatizar el proceso de actualizar dos cuentas diferentes en cuanto a sus transacciones.",
                "Implementé un bot de Telegram que mantiene un seguimiento de un sistema interno de puntos en Integromat utilizando las APIs de Telegram y Google sheets."
            ]
        }
    ]
}