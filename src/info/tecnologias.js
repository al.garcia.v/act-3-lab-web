export default function tecnologias() {
    return [
        {
            "nombre": "C++",
            "habilidad": "intermedio",
            "tipo": "lenguaje",
            "foto": "./assets/logos/cpp.png"
        },
        {
            "nombre": "Java",
            "habilidad": "principiante",
            "tipo": "lenguaje",
            "foto": "./assets/logos/java.png"
        },
        {
            "nombre": "Javascript",
            "habilidad": "intermedio",
            "tipo": "lenguaje",
            "foto": "./assets/logos/js.png"
        },
        {
            "nombre": "Python",
            "habilidad": "principiante",
            "tipo": "lenguaje",
            "foto": "./assets/logos/python.png"
        },
        {
            "nombre": "Git",
            "habilidad": "intermedio",
            "tipo": "tecnologia",
            "foto": "./assets/logos/git.png"
        },
        {
            "nombre": "CSS",
            "habilidad": "principiante",
            "tipo": "tecnologia",
            "foto": "./assets/logos/css.png"
        },
        {
            "nombre": "NodeJS",
            "habilidad": "intermedio",
            "tipo": "tecnologia",
            "foto": "./assets/logos/node.png"
        },
        {
            "nombre": "MongoDB",
            "habilidad": "principiante",
            "tipo": "tecnologia",
            "foto": "./assets/logos/mongo.png"
        },
        {
            "nombre": "Android Studio",
            "habilidad": "principiante",
            "tipo": "tecnologia",
            "foto": "./assets/logos/android.png"
        },
        {
            "nombre": "HTML",
            "habilidad": "intermedio",
            "tipo": "tecnologia",
            "foto": "./assets/logos/html.png"
        },
        {
            "nombre": "Godot",
            "habilidad": "intermedio",
            "tipo": "tecnologia",
            "foto": "./assets/logos/godot.png"
        },
        {
            "nombre": "React",
            "habilidad": "principiante",
            "tipo": "tecnologia",
            "foto": "./assets/logos/react.png"
        },
        {
            "nombre": "PostreSQL",
            "habilidad": "principiante",
            "tipo": "tecnologia",
            "foto": "./assets/logos/postgres.png"
        }
    ]
}