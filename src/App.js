import React, { useState, useEffect } from 'react';

// import axios from 'axios';

import NavbarPersonal from "./sections/NavbarPersonal"
import Name from "./sections/Name"
import AboutMe from "./sections/AboutMe"
import Education from "./sections/Education"
import Technologies from "./sections/Technologies"
import Works from "./sections/Works"
import Projects from "./sections/Projects"
import Footer from "./sections/Footer"

import proyectos from './info/proyectos';
import trabajos from './info/trabajos';
import tecnologias from './info/tecnologias';

function App() {

    const [listaProyectos, setListaProyectos] = useState([]);

    const [listaTrabajos, setListaTrabajos] = useState([]);

    const [listaTecnologias, setListaTecnologias] = useState([])

    useEffect(() => {
        // const fetchData = async () => {
        //     const res = await axios(`./json/proyectos.json`);
        //     setListaProyectos(res.data);

        //     const res2 = await axios(`./json/trabajos.json`);
        //     setListaTrabajos(res2.data);

        //     const res3 = await axios(`./json/tecnologias.json`);
        //     setListaTecnologias(res3.data);

        //     console.log(res, res2, res3)
        // };
    
        // fetchData();
        setListaProyectos(proyectos());
        setListaTrabajos(trabajos());
        setListaTecnologias(tecnologias());
    }, []);

    return (
        <div className="d-flex flex-column h-100">
            <NavbarPersonal />
            <main className="flex-shrink-0">
                <Name />
                <AboutMe />
                <Education />
                <Technologies
                    technologies={listaTecnologias}
                />
                <Works 
                    works={listaTrabajos}
                />
                <Projects 
                    projects={listaProyectos}
                />
            </main>
            <Footer />
        </div>
    );
}

export default App;
