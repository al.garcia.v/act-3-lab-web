import React from 'react'
import { Navbar, Nav } from 'react-bootstrap';

export default function NavbarPersonal() {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="#home">Al García</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#About">Sobre mí</Nav.Link>
                    <Nav.Link href="#Tecnologias">Tecnologías</Nav.Link>
                    <Nav.Link href="#Experiencia">Trabajos</Nav.Link>
                    <Nav.Link href="#Proyectos">Proyectos</Nav.Link>
                    <Nav.Link href="#Credits">Contacto</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
