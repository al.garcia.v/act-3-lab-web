import React from 'react'

export default function AboutMe() {
    return (
        <section className="section" id="About">
            <div className="container">
                <h2 className="title has-text-centered is-2">Sobre mí</h2>
                <p className="content is-large">
                    Buenas, mi nombre es Al García, soy un estudiante mexicano de ciencias computacionales.
                    Me gusta mucho el desarrollo web y de videojuegos. Aparte de programar, me gusta la escritura.
                </p>
            </div>
        </section>
    )
}
