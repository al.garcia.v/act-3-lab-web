// import { useEffect, useState } from 'react';
import { Row } from 'react-bootstrap';

import Techonology from './../components/Techonology';

export default function Technologies(props) {
    const { technologies } = props;

    // const [lenguajes, setLenguajes] = useState([]);
    // const [tecnologias, setTecnologias] = useState([]);
    // const [otros, setOtros] = useState([]);

    // useEffect(() => {
    //     let lista = technologies.filter(tech => (tech.tipo == 'lenguaje' && tech.habilidad == "intermedio"));
    //     console.log(lista);
    //     setLenguajes(lista);
    // }, [technologies])

    // useEffect(() => {
    //     let lista = technologies.filter(tech => (tech.tipo == 'tecnologia' && tech.habilidad == "intermedio"));
    //     console.log(lista);
    //     setTecnologias(lista);
    // }, [])

    // useEffect(() => {
    //     let lista = technologies.filter(tech => (tech.habilidad != "intermedio"));
    //     console.log(lista);
    //     setOtros(lista);
    // }, [])

    return (
        <section className="section has-text-centered " id="Tecnologias">
            <div className="container content is-medium" id="columnasTecnologias">
                <h2 className="title is-2">Tecnologias</h2>
                {/* <h3 className="title is-3">Lenguajes</h3> */}
                {/* <TechList
                    techs={technologies}
                    chunks={4}
                /> */}
                <Row>
                    {
                        technologies.map((tech, index) => (
                            <Techonology
                                key={index}
                                tech={tech}
                            />
                        ))
                    }
                </Row>
                {/* <div className="section" id="lenguajes"></div>
                <h3 className="title is-3">Tecnologías</h3>
                <TechList
                    techs={technologies}
                    chunks={4}
                />
                <div className="section" id="tecnologias"></div>
                <h4 className="title is-4">Otras</h4>
                <TechList
                    techs={technologies}
                    chunks={5}
                />
                <div className="section" id="otras"></div> */}
            </div>
        </section>
    )
}

// function TechList(props) {
//     const {techs, chunk} = props;

//     // const [lista, setLista] = useState([]);

//     // useEffect(() => {
//     //     let arr = []
//     //     for (let i = 0, j = techs.length; i < j; i += chunk) {
//     //         arr.push(techs.slice(i, i + chunk));
//     //     }
//     //     setLista(arr);
//     // }, [])

//     return (
//         <div>
//             {/* {
//                 lista.map((techArr, index) => (
//                     <Row key={index}>
//                         {
//                             techArr.map((tech, index) => (
//                                 <Col key={index}>
//                                     <Techonology
//                                         key={index}
//                                         tech={tech}
//                                     />
//                                 </Col>
//                             ))
//                         }
//                     </Row>
//                 ))
//             } */}
//             {techs.map((tech, index) => (
//                 <Techonology
//                     key={index}
//                     tech={tech}
//                 />
//             ))}
//         </div>
//     )
// }
