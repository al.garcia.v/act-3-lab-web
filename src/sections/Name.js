import React from 'react'

export default function Name() {
    return (
        <div className="container text-center">
            <h1 className="mt-5">Al García</h1>
            <p className="lead"> Estudiante de 8vo semestre de Ciencias Computacionales</p>
        </div>
    )
}
