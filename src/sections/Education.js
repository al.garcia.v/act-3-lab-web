import React from 'react'

export default function Education() {
    return (
        <section>
            <div className="container text-center">
                <h2 className="title is-2">Educación</h2>
                <div>
                    <p>Instituto Tecnológico y de Estudios Superiores de Monterrey Campus Monterrey</p>
                    <p>Ingeniería en Tecnologías Computacionales (ITC)</p>
                    <p>Agosto 2017 - Presente</p>
                    <p>Fecha de graduación estimada: Diciembre 2021</p>
                    <p>Promedio: 89/100</p>
                </div>
            </div>
        </section>
    )
}
