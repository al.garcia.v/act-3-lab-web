// import { useState, useEffect } from 'react';
// import { Col, Container, Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';

import Project from './../components/Project'

export default function Projects(props) {
    const { projects } = props;

    return (
        <Container>
            <section className="section" id="Proyectos">
                <h2 className="title has-text-centered is-2">Proyectos</h2>
                {
                    projects.map((project, index) => (
                        <Project
                            key={index}
                            project={project}
                        />
                    ))
                }
                {/* <ProjectList 
                    projects={projects}
                /> */}
            </section>
        </Container>
    )
}

// function ProjectList(props) {
//     const {projects} = props;

//     const [lista, setLista] = useState([]);

//     useEffect(() => {
//         var chunk = 3;
//         let arr = []
//         for (let i = 0, j = projects.length; i < j; i += chunk) {
//             arr.push(projects.slice(i, i + chunk));
//         }
//         setLista(arr);
//     }, [])

//     return (
//         <div>
//             {
//                 lista.map((projectArr, index) => (
//                     <Row>
//                         {
//                             projectArr.map((project, index) => (
//                                 <Col>
//                                     <Project
//                                         key={index}
//                                         project={project}
//                                     />
//                                 </Col>
//                             ))
//                         }
//                     </Row>
//                 ))
//             }
//         </div>
//     )
// }