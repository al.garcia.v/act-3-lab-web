// import { useState, useEffect } from 'react';
// import { Col, Container, Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';

import Work from './../components/Work';

export default function Works(props) {
    const { works } = props;

    return (
        <Container>
            <section className="section" id="Experiencia">
                <h2 className="title is-2">Experiencia Laboral</h2>
                {
                    works.map((work, index) => (
                        <Work
                            key={index}
                            work={work}
                        />
                    ))
                }
                {/* <WorkList 
                    works={works}
                /> */}
            </section>
        </Container>
    )
}

// function WorkList(props) {
//     const {works} = props;

//     const [lista, setLista] = useState([]);

//     useEffect(() => {
//         var chunk = 3;
//         let arr = []
//         for (let i = 0, j = works.length; i < j; i += chunk) {
//             arr.push(works.slice(i, i + chunk));
//         }
//         setLista(arr);
//     }, [])

//     return (
//         <div>
//             {
//                 lista.map((workArr, index) => (
//                     <Row>
//                         {
//                             workArr.map((work, index) => (
//                                 <Col>
//                                     <Work
//                                         key={index}
//                                         work={work}
//                                     />
//                                 </Col>
//                             ))
//                         }
//                     </Row>
//                 ))
//             }
//         </div>
//     )
// }