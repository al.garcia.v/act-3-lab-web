import React from 'react'
import { Navbar } from 'react-bootstrap';

export default function Footer() {
    return (
        <footer id="Credits">
            <Navbar bg="dark" variant="dark" >
                <div className="container d-flex justify-content-center">
                    <div className="text-light text-center">
                        <p> <strong>Correo:</strong> albertogarv1402@gmail.com </p> 
                        <p> <a href="https://gitlab.com/al.garcia.v" target="_blank" rel="noreferrer"> Gitlab </a> </p>
                        <h6> ©2021 | Alberto García </h6>
                    </div>
                </div>
            </Navbar>
        </footer>
    )
}
